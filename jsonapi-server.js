var jsonApi = require("jsonapi-server");

jsonApi.setConfig({
  port: 3000,
});

jsonApi.define({
  resource: "owners",
  handlers: jsonApi.mockHandlers,
  attributes: {
    name: jsonApi.Joi.string(),
    timestamp: jsonApi.Joi.number().precision(0)
  }
});
// jsonApi.define({
//   resource: "pets",
//   handlers: jsonApi.mockHandlers,
//   attributes: {
//     name: jsonApi.Joi.string(),
//     owner: jsonApi.Joi.one("owner")
//   }
// });

// jsonApi.define({
//   resource: "dogs",
//   handlers: jsonApi.mockHandlers,
//   attributes: {
//     name: jsonApi.Joi.string(),
//     owner: jsonApi.Joi.one("owner")
//   }
// });

// jsonApi.define({
//   resource: "cats",
//   handlers: jsonApi.mockHandlers,
//   attributes: {
//     name: jsonApi.Joi.string(),
//     owner: jsonApi.Joi.one("owner")
//   }
// });

jsonApi.start();
