import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  // this.route('pets', function() {
  //   this.route('new');
  //   this.route("edit", {
  //     path: ":pet_id/edit"
  //   });
  //   this.route("show", {
  //     path: ":pet_id"
  //   });
  // });
  this.route('pet', { path: '/pet/:pet_id' });

  this.route('pets', function() {
    this.route('new');
    this.route('pet', { resetNamespace: true, path: ':pet_id' }, function() {
      this.route("edit", { path: "edit" });
      //this.route('show', { path: ":pets_id" });
    });
  });

  this.route('owners', function() {
    this.route('new');
    this.route('owner', { resetNamespace: true, path: ':owner_id' }, function() {
      this.route("edit", { path: "edit" });
      this.route('index', { path: "/" }, function() {
        this.route('pets', { resetNamespace: true }, function() {
          this.route('new');
          this.route('pet', { resetNamespace: true, path: ':pet_id' }, function() {
            this.route("edit", { path: "edit" });
            //this.route('show', { path: ":pets_id" });
          });
        });
      });
    });
    // this.route("edit", {
    //   path: ":owner_id/edit"
    // });
    // this.route("show", {
    //   path: "show/:owner_id"
    // });
  });
});

export default Router;
