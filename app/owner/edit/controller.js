import Ember from 'ember';

// app/controllers/posts.js
export default Ember.Controller.extend({
  actions: {
    save: function() {
      this.get('model').save().then( () => {
        this.transitionToRoute('owner', this.get('model'));
      });
    }
  }
});
