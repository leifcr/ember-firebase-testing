import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  timestamp: DS.attr('number'),
  pets: DS.hasMany('pet', {async: true} )
});
