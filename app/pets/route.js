import Ember from 'ember';

export default Ember.Route.extend({
  sortProperties: ['timestamp'],
  sortAscending: false, // sorts post by timestamp
  model: function() {
    return this.store.findAll('pet');
  }
});
