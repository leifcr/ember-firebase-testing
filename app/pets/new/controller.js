import Ember from 'ember';

// app/controllers/posts.js
export default Ember.Controller.extend({
  ownerController: Ember.inject.controller('owner'),
  owner: Ember.computed.reads('ownerController.model'),
  pet_types: ['dog', 'cat', 'elephant', 'rhino'],
  init: function() {
    this.set('pet',  Ember.Object.create());
  },
  actions: {
    save: function(params) {
      var name = this.get('pet.name');
      if (!name.trim()) { return; }
      var type = this.get('pet.type');
      if (!type.trim()) { return; }
      // Should validate type here!
      // Set owner
      //
      // var owner = this.get('owner');
      // var owner = this.store.findRecord('owner', params.owner_id);

      var newPet = this.store.createRecord('pet', {
        name: name,
        timestamp: new Date().getTime(),
        owner: this.get('owner'),
        type: type,
      });
      newPet.save().then( () => {
        // this.transitionToRoute('owner.show', newOwner);
      });
    }
  }
});
