import Pet from 'ember-fire-polymorph/pet/model';

export default Pet.extend({
  type: 'dog'
});
