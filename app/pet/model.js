import DS from 'ember-data';

const Pet = DS.Model.extend({
  name: DS.attr('string'),
  type: DS.attr('string'),
  // type: DS.attr('string'),
  owner: DS.belongsTo('owner', {polymorphic: true})
});

export default Pet;