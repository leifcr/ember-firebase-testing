import Ember from 'ember';

// app/controllers/posts.js
export default Ember.Controller.extend({
  init: function() {
    this.set('owner',  Ember.Object.create());
  },
  actions: {
    save: function() {
      var name = this.get('owner.name');
      if (!name.trim()) { return; }

      var newOwner = this.store.createRecord('owner', {
        name: name,
        timestamp: new Date().getTime()
      });
      newOwner.save().then( () => {
        this.transitionToRoute('owner', newOwner);
      });
    }
  }
});
