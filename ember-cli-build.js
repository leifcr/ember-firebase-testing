/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    // Add options here
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  app.import('bower_components/Materialize/dist/js/materialize.js');
  app.import('bower_components/sweetalert/dist/sweetalert.min.js');
  app.import('bower_components/sweetalert/dist/sweetalert.css');

  app.import('bower_components/mdi/fonts/materialdesignicons-webfont.svg', {destDir: 'assets/fonts'} );
  app.import('bower_components/mdi/fonts/materialdesignicons-webfont.eot', {destDir: 'assets/fonts'} );
  app.import('bower_components/mdi/fonts/materialdesignicons-webfont.ttf', {destDir: 'assets/fonts'} );
  app.import('bower_components/mdi/fonts/materialdesignicons-webfont.woff', {destDir: 'assets/fonts'} );
  app.import('bower_components/mdi/fonts/materialdesignicons-webfont.woff2', {destDir: 'assets/fonts'} );

  // app.import('bower_components/Materialize/dist/font/material-design-icons/Material-Design-Icons.svg', {destDir: 'assets/fonts'} );
  // app.import('bower_components/Materialize/dist/font/material-design-icons/Material-Design-Icons.eot', {destDir: 'assets/fonts'} );
  // app.import('bower_components/Materialize/dist/font/material-design-icons/Material-Design-Icons.ttf', {destDir: 'assets/fonts'} );
  // app.import('bower_components/Materialize/dist/font/material-design-icons/Material-Design-Icons.woff', {destDir: 'assets/fonts'} );
  // app.import('bower_components/Materialize/dist/font/material-design-icons/Material-Design-Icons.woff2', {destDir: 'assets/fonts'} );

  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Bold.ttf', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Bold.woff', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Bold.woff2', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Light.ttf', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Light.woff', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Light.woff2', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Medium.ttf', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Medium.woff', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Medium.woff2', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Regular.ttf', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Regular.woff', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Regular.woff2', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Thin.ttf', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Thin.woff', {destDir: 'assets/fonts'} );
  app.import('bower_components/Materialize/dist/font/roboto/Roboto-Thin.woff2', {destDir: 'assets/fonts'} );

  return app.toTree();
};
